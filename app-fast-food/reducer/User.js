import * as actions from "../actions/user";
const DEFAULT_STATE = {
  isLoading: false,
  data: "",
  dataSearch:'',
  stateRegister:false,
  error: false,
  messageError: "",
};

export default (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case actions.signinRequest.type:
    case actions.signupRequest.type:
    case actions.searchFoodRequest.type:
      return {
        ...state,
        isLoading: true,
      };
    case actions.signinSuccess.type:
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      };
    case actions.signupSuccess.type:
      return {
        ...state,
        isLoading: false,
        stateRegister:true
      };
      case actions.searchFoodSuccess.type:
        return {
            ...state,
            isLoading:false,
            dataSearch:action.payload

        }
    case actions.signinFailure.type:
    case actions.signupFailure.type:
    case actions.searchFoodFailure.type:
      return {
        ...state,
        isLoading: false,
        error: true,
        messageError: action.payload,
      };

    default:
      return state;
  }
};

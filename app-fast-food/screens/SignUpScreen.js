import React from "react";
import { connect } from "react-redux";
import logo from "../assets/logo.jpg"
import { StyleSheet } from "react-native";
import *as actions from '../actions/user'
import {
  Box,
  Text,
  Input,
  Stack,
  Center,
  NativeBaseProvider,
  Heading,
  Button,
  Image,
  Divider,
  Link,
} from "native-base";
import validate from '../ultis/validate'
export const SignUpScreen = (props) => {
  const [newAccount, setNewAccount] = React.useState({
    name: "", phone: "", email: "", password: ""
  })
  return (
    <NativeBaseProvider>
      <Center>
        <Box margin={60}>
          <Image
            justifyContent="center"
            source={logo}
            w={60}
            h={60}
            borderRadius={10}
            alt="logo"
          />
          <Text>Eating now</Text>
        </Box>
        <Text fontSize={30}>let's sign you in</Text>
        <Heading size={"xl"} style={styles.label1}>
          Sign Up
        </Heading>
        <Stack space={5} w={"90%"}>
          <Input
            variant="rounded"
            style={styles.input}
            placeholder="Enter your name"
            onChange={(text) => {
              setNewAccount({
                ...newAccount,name:text
              })
             }}
          />
          <Input
            variant="rounded"
            style={styles.input}
            placeholder="Enter your email"
            onChange={(text) => {
              setNewAccount({
                ...newAccount,
                email:text
              })
             }}
          />
          <Input
            variant="rounded"
            style={styles.input}
            placeholder="Enter your phone"
            onChange={(text) => { setNewAccount({
                ...newAccount,
                phone:text
              })}}
          />
          <Input
            variant="rounded"
            style={styles.input}
            placeholder="Enter your password"
            onChangeText={(text) => {
              setNewAccount({
                ...newAccount,
                password:text
              })
             }}
          />
          <Button
            style={styles.btn}
            onPress={(e) => {
              props.signup(newAccount);
            }}
          >
            Sign Up
          </Button>
          <Divider />

        </Stack>
        <Text style={styles.txt}>Have an account ? <Text styles={styles.txtPress} color="blue.500" onPress={(e) => {
          props.navigation.navigate('SignIn')
        }}>Sign In</Text> </Text>
      </Center>
    </NativeBaseProvider >
  );
};

const mapStateToProps = (state) => {
  return {
    stateUser :state.UserReducer.stateRegister
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    signup:(data)=>{
      dispatch(actions.signupRequest(data))
    }
  };
};
const styles = StyleSheet.create({
  input: {
    margin: 5,
    height: 40

  },
  label1: {
    marginBottom: 30
  },
  label2: {
    fontSize: 30,

  }, txt: {
    marginTop: 30,
    fontSize: 20
  },
  txtPress: {

  },

  btn: {
    borderRadius: 20,
    height: 50,
    backgroundColor: '#ff9800',

  }
})
export default connect(mapStateToProps, mapDispatchToProps)(SignUpScreen);

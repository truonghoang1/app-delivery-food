import React from 'react'
import { connect, useDispatch } from 'react-redux'
import { StyleSheet } from "react-native"
import { Box, Text, Input, Stack, Center, NativeBaseProvider, Heading, Button, Image, Divider, Link } from 'native-base'
import AsyncStorage from '@react-native-async-storage/async-storage'
import logo from "../assets/logo.jpg"
import *as actions from '../../actions/user'
export const SignInScreen = (props) => {
  const ref = React.useRef(null)

  const [login, setLogin] = React.useState({
    email: "", password: ""
  })
  const dispatch = useDispatch()
  React.useEffect(() => {
    AsyncStorage.getItem('location', (err, data) => {
      err ? console.log(err) : console.log("daata ne", data);;
    })
  }, [])
  // console.log(props);
  React.useEffect(()=>{
if(props.useState!==""){
  AsyncStorage.setItem('token',props.userState)
  props.navigation.navigate('Home')
}
  },[props.userState])
  return (
    <NativeBaseProvider  >

      <Center    >

        <Box margin={60} >
          <Image justifyContent="center" alt={"logo"} source={logo} w={60} h={60} borderRadius={10} />
          <Text>Eating now</Text>

        </Box>
        <Text fontSize={30}>let's sign you in</Text>
        <Heading size={'xl'} style={styles.label1}>Sign In</Heading>
        <Stack space={5} w={"90%"} >

          <Input variant='rounded' style={styles.input} placeholder="Enter your email" onChangeText={(text) => {
            setLogin({ ...login, email: text })
          }} />
          <Input variant='rounded' style={styles.input} placeholder="Enter your password" onChangeText={(text) => {
            setLogin({ ...login, password: text })
          }} />
          <Button style={styles.btn} ref={ref} onPress={() => {
            // props.loginRequest(login)
            console.log("hello baby");
            dispatch(actions.signinRequest(login))


          }}>Sign In</Button>
          <Divider />

        </Stack>

        <Text style={styles.txt}>Don't have an account ?<Text styles={styles.txtPress} color='blue.500' onPress={(e) => {
          props.navigation.navigate('SignUp')
        }}>SignUp</Text> </Text>
      </Center>

    </NativeBaseProvider>

  )
}

const mapStateToProps = (state) => {
  return {
    userState: state.UserReducer.data

  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    loginRequest: (data) => {
      dispatch(actions.signinRequest(data))
    }
  }
}
const styles = StyleSheet.create({
  input: {
    margin: 5,
    height: 40

  },
  label1: {
    marginBottom: 30
  },
  label2: {
    fontSize: 30,

  }, txt: {
    marginTop: 30,
    fontSize: 20
  },
  txtPress: {
    color: '#2196f3'
  },

  btn: {
    borderRadius: 20,
    height: 50,
    backgroundColor: '#ff9800',

  }
})

export default connect(mapStateToProps, mapDispatchToProps)(SignInScreen)
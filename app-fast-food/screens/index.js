import OnBoardingScreen from "./OnBoardingScreen";
import HomeScreen from "./HomeScreen";
import DetailScreen from "./DetailScreen";
import SignInScreen from "./signin/MainScreen";
import SignUpScreen from "./SignUpScreen";
import MyDiscountsScreen from "./MyDiscountsScreen";
export  {
OnBoardingScreen,HomeScreen,DetailScreen,SignInScreen,SignUpScreen,MyDiscountsScreen
}
export function validate(type,value){
    let err=""
    let flag =true
    const testEmail =/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/i;
    const testPhoneNumber = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    const regexPw = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*\W)/
    switch (type) {
        case 'phone':
            if(!testPhoneNumber.test(value)){
                return {
                    flag:false,
                    err:'Phone number type is not valid'
                }
            }else{
                return {
                    flag:true,
                    err:''
                }
            }
        case 'email':
            if(!testEmail.test(value)){
                return {
                    flag:false,
                    err:'Email type is not valid'
                }
            }else{
                return {
                    flag:true,
                    err:''
                }
            }
        case 'password':
            if(!regexPw.test(value)){
                return {
                    flag:false,
                    err:'character must [a-z][A-Z][1-9][@,#.%]'
                }
            }else{
                return {
                    flag:true,
                    err:''
                }
            }
            
          
    
        default: return {flag:true,err:''}
          
    }
    

}
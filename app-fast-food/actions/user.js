import { createAction } from "@reduxjs/toolkit"
const HEADER_SIGNIN = 'sign-in'
const HEADER_SIGNUP = 'sign-up'
const HEADER_USER ='user-action'
const signinRequest = createAction(`${HEADER_SIGNIN}-request`)
const signinSuccess = createAction(`${HEADER_SIGNIN}-Success`)
const signinFailure = createAction(`${HEADER_SIGNIN}-Failure`)

const signupRequest =createAction(`${HEADER_SIGNUP}-request`)
const signupSuccess =createAction(`${HEADER_SIGNUP}-success`)
const signupFailure = createAction(`${HEADER_SIGNUP}-failure`)

const searchFoodRequest = createAction(`${HEADER_USER}-search-request`)
const searchFoodSuccess = createAction(`${HEADER_USER}-search-success`)
const searchFoodFailure = createAction(`${HEADER_USER}-search-failure`)



export {
    signinRequest, signinFailure, signinSuccess, signupFailure, signupRequest,
    signupSuccess, searchFoodFailure, searchFoodRequest, searchFoodSuccess
}
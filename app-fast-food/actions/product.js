import {createAction} from '@reduxjs/toolkit'
const HEADER_F= 'product_favorite'
const HEADER_O = 'product_order_many'

const getFFRequest =createAction(`get-${HEADER_F}-request`)
const getFFSuccess =createAction(`get-${HEADER_F}-success`)
const getFFFailure =createAction(`get-${HEADER_F}-failure`)

const getFORequest = createAction(`get-${HEADER_O}-request`)
const getFOSuccess = createAction(`get-${HEADER_O}-success`)
const getFOFailure = createAction(`get-${HEADER_O}-failure`)

export{
    getFFFailure,getFFRequest,getFFSuccess,
    getFOFailure,getFORequest,getFOSuccess
}

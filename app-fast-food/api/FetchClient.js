const baseURL = 'http://192.168.0.105:3001/';

const handleResult = (data) => {

    return data;
}

const fetchRequest = (url, method, data) => {
    const promise = new Promise((resolve, reject) => {
        url = `${baseURL}${url}`;
    console.log(url);
        const config = {
            method,
            headers: {
                "Content-Type": "application/json",
                // "Access-Control-Allow-Origin": "*",
                // "Access-Control-Allow-Headers": "*",
            },
            redirect: 'follow',
            body: JSON.stringify(data)
        }
    
       
    
        fetch(url, config)
            .then(response => response.json())
            .then(data => {
                console.log(data);
                resolve(handleResult(data));
            })
            .catch(error => {
                console.log(error);
                reject(error);
            })
    });

    return (promise);
}

const ApiClient = {
    get: (url, payload) => fetchRequest(url, 'GET', payload),
    post: (url, payload) => fetchRequest(url, 'POST', payload),
    put: (url, payload) => fetchRequest(url, 'PUT', payload),
    path: (url, payload) => fetchRequest(url, 'PATH', payload),
    delete: (url, payload) => fetchRequest(url, 'DELETE', payload),
};

export default ApiClient;
import axios from "axios";

import queryString from "query-string";

import moment from "moment";
// import *as AsyncStorage from "expo-secure-store"
// import { useToast } from "native-base"
// const toast = useToast()
function createAxios() {
  var axiosInstant = axios.create();
  axiosInstant.defaults.baseURL = "http://localhost:3001";
  // axiosInstant.defaults.baseURL = "http://localhost:8888";
  axiosInstant.defaults.timeout = 20000;
  axiosInstant.defaults.headers = { "Content-Type": "application/json" };
  axiosInstant.defaults.headers = { "access-control-allow-origin": "*" };

  axiosInstant.interceptors.request.use(
    async (config) => {
      // config.headers.token = AsyncStorage.getItemAsync("token");
      // Cookie.get('SESSION_ID')
      return config;
    },
    (error) => {
      Promise.reject(error);
    }
  );
  axiosInstant.interceptors.response.use(
    (response) => {
      //   if (!process.env.NODE_ENV || process.env.NODE_ENV === "development") {
      //     // Reactotron.apisauce(response);
      //   } else {
      //     // production code
      //   }

      if (response?.data?.code === 403) {
        // AsyncStorage.deleteItemAsync("token")

      } else if (response?.data?.status !== 1) {

        // toast.show({ description: res.data.message })
      }
      return response;
    },
    (error) => {

      if (error?.response?.data?.code === 403) {
        // AsyncStorage.deleteItemAsync("token")
        // toast.show({ description: res.data.message })
      } else {
        // AsyncStorage.deleteItemAsync("token")
      }
    }
  );
  return axiosInstant;
}

export const axiosClient = createAxios();

/* Support function */
function handleResult(api) {
  return api.then((res) => {
    // alert(JSON.stringify(res.data));
    if (res?.data?.status !== 1) {
      if (res?.data?.code === 403) {
        // AsyncStorage.deleteItemAsync("token")
        // toast.show({ description: res.data.message })
      }
      return Promise.reject(res?.data);
    }
    return Promise.resolve(res?.data);
  });
}
// handle url
function handleUrl(url, query) {
  return queryString.stringifyUrl({ url: url, query });
}
// export file xlsx
export function exportXlsx(url, payload) {
  const hostName = "http://192.168.0.105:3001/";

  axios
    .post(
      `${hostName + url}?start=${payload.start}&&end=${payload.end}`,
      payload.opsSkip,
      { responseType: "blob" }
    )
    .then(function (response) {
      // console.log(response);
      let fileName = `${moment(new Date()).format("YYYY-MM-DD")}`;
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        // IE variant
        window.navigator.msSaveOrOpenBlob(
          new Blob([response.data], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
          }),
          fileName
        );
      } else {
        const url = window.URL.createObjectURL(
          new Blob([response.data], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
          })
        );
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute(
          "download",
          fileName
        );
        document.body.appendChild(link);
        link.click();
      }
    });
}
export default ApiClient = {

  get: (url, payload) => handleResult(axiosClient.get(handleUrl(url, payload))),
  post: (url, payload) => handleResult(axiosClient.post(url, payload)),
  put: (url, payload) => handleResult(axiosClient.put(url, payload)),
  path: (url, payload) => handleResult(axiosClient.patch(url, payload)),
  delete: (url, payload) => handleResult(axiosClient.delete(url, payload))
}



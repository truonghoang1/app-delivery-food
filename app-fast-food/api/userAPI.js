// import ApiClient from './index'
import fetchAPI from "./FetchClient"
export const signUp = payload => fetchAPI.post(`api/user/signup`, payload)
export const signIn = (payload) => fetchAPI.post(`api/user/signin`, payload)
export const searchFood = payload => fetchAPI.post(`api/user/search`,payload)

import React from 'react'
import { Platform, DeviceEventEmitter, NativeModules, NativeEventEmitter, View, Button } from 'react-native';
import RNMomosdk from 'react-native-momosdk';
const EventEmitter = new NativeEventEmitter(NativeModules.RNMomosdk);
const merchantname = "Fast-food store";
const merchantcode = "MOMOXDOS20220916";
const merchantNameLabel = "Nhà cung cấp";
const billdescription = "FThanh toan ne";
const amount = 50000;
const enviroment = "0";

function ButtonPayMomo() {
    React.useEffect(() => {
        EventEmitter.addListener('RCTMoMoNoficationCenterRequestTokenReceived', (response) => {
            try {
                console.log("<MoMoPay>Listen.Event::" + JSON.stringify(response));
                if (response && response.status == 0) {
                    //SUCCESS: continue to submit momoToken,phonenumber to server
                    let fromapp = response.fromapp; //ALWAYS:: fromapp==momotransfer
                    let momoToken = response.data;
                    let phonenumber = response.phonenumber;
                    let message = response.message;
                } else {
                    //let message = response.message;
                    //Has Error: show message here
                }
            } catch (ex) { }
        });

        EventEmitter.addListener('RCTMoMoNoficationCenterRequestTokenState', (response) => {
            console.log("<MoMoPay>Listen.RequestTokenState:: App is not installed or config LSApplicationQueriesSchemes failed");
            //response.status = 1: Parameters valid & ready to open MoMo app., 2: canOpenURL failed for URL MoMo app, 3: Parameters invalid
        })
    }, [])
    const onPress = async () => {
        let jsonData = {};
        jsonData.enviroment = "1"; //"0": SANBOX , "1": PRODUCTION
        jsonData.action = "gettoken"; //DO NOT EDIT
        jsonData.merchantname = merchantname; //edit your merchantname here
        jsonData.merchantcode = merchantcode; //edit your merchantcode here
        jsonData.merchantnamelabel = merchantNameLabel;
        jsonData.description = billdescription;
        jsonData.amount = 5000;//order total amount
        jsonData.orderId = "ID20181123192300";
        jsonData.orderLabel = "Ma don hang";
        jsonData.appScheme = "momoxdos20220916";// iOS App Only , match with Schemes Indentify from your  Info.plist > key URL types > URL Schemes
        // if (Platform.OS === 'android') {
        let dataPayment = await RNMomosdk.requestPayment(jsonData);
        momoHandleResponse(dataPayment);
        // } else {
        // RNMomosdk.requestPayment(jsonData);
        // }
    }

    function momoHandleResponse(response) {
        try {
            if (response && response.status == 0) {
                //SUCCESS continue to submit momoToken,phonenumber to server
                let fromapp = response.fromapp; //ALWAYS:: fromapp == momotransfer
                let momoToken = response.data;
                let phonenumber = response.phonenumber;
                let message = response.message;

            } else {
                //let message = response.message;
                //Has Error: show message here
            }
        } catch (ex) { }
    }
    return (
        <View>
            <Button onPress={onPress}>
                Payment by momo
            </Button>

        </View>
    )
}

export default ButtonPayMomo
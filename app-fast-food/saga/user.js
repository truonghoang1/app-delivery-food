import { put, takeEvery, takeLatest } from 'redux-saga/effects'
import *as actions from '../actions/user'
// import { fetchAPI } from '../api/fetchAPI'
import { signIn,signUp,searchFood } from '../api/userAPI'

function* signinSaga({payload}) {
    // try {
    
    const res = yield signIn(payload)
    console.log("res ne", res);
    if (res.code !== 200) {
        yield put(actions.signinFailure(res.message))
    } else {
        yield put(actions.signinSuccess(res.data))
    }

    // } catch (error) {
    //     console.log("error rooi");
    // }
}
function* signUpSaga({payload}){
    const res = yield signUp(payload) 
    if(res.code !==200){
        yield put(actions.signupFailure(res.message))
    }else{
        yield put(actions.signupSuccess(res.data))
    }
}
function* searchFoodSaga({payload}){
    const res = yield searchFood(payload)
    if(res.code !==200){
        yield put(actions.searchFoodFailure(res.message))

    }else{
        yield put(actions.searchFoodSuccess(res.data))
    }
}

export default function* watchAccount() {
    yield takeLatest(actions.signinRequest.type, signinSaga)
    yield takeLatest(actions.signupRequest,signUpSaga)
    yield takeLatest(actions.searchFoodRequest,searchFoodSaga)

}

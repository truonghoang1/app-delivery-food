import { all } from "redux-saga/effects"
import watchAccount from "./user"

export default function* rootSaga() {
    // yield console.log("chao cac ban");
    yield all([
        watchAccount()
    ])
}
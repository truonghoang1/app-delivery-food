import React from "react"
import { createAppContainer } from "react-navigation"
import { createStackNavigator } from "react-navigation-stack"
import { Provider } from "react-redux"
// import store from "./store"
import *as screen from "./screens"
import AsyncStorage from "@react-native-async-storage/async-storage"

import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';

import createSagaMiddleware from "redux-saga"
import reducer from './reducer'
import saga from './saga'


export default function App() {
  const [isAppFirstLaunched, setIsAppFirstLaunched] = React.useState(null);
  const sagaMiddleware = createSagaMiddleware();
  const store = configureStore({
    reducer: reducer,
    middleware: getDefaultMiddleware =>
      getDefaultMiddleware().concat(sagaMiddleware)
  });
  sagaMiddleware.run(saga);
  React.useEffect(async() => {
    const storeApp = await AsyncStorage.getItem("isAppFirstLaunched")
    if (storeApp == null) {
      setIsAppFirstLaunched(true);
      AsyncStorage.setItem("isAppFirstLaunched", "false")

    } else {
      setIsAppFirstLaunched(false);
    }

     AsyncStorage.removeItem('isAppFirstLaunched');
  }, []);
  const AppNavigator = createStackNavigator({
    OnBoarding: {
      screen: screen.OnBoardingScreen,
      navigationOptions: {
        headerShown: false
      }

    },
    Home: {
      screen: screen.HomeScreen,
      navigationOptions: {
        headerShown: false
      }

    },
    SignIn: {
      screen: screen.SignInScreen,
      navigationOptions: {
        headerShown: false,
      }

    },
    SignUp: {
      screen: screen.SignUpScreen,
      navigationOptions: {
        headerShown: false
      }

    },
    Detail: screen.DetailScreen,
    Discounts: screen.MyDiscountsScreen
  }, {
      initialRouteName: isAppFirstLaunched? "OnBoarding":"SignIn"
    // initialRouteName: "OnBoarding"
  }
  )
  const AppContainer = createAppContainer(AppNavigator)
  return (
    // <SafeAreaProvider >
    <Provider store={store}>
      <AppContainer />
    </Provider>
    // </SafeAreaProvider>

  );
}


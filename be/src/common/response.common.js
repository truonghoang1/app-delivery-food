const success =(res,code,message ,data) =>{
    res.status(code).json({
     status:1,
     code:code || 200,
     message:message || 'success' ,
     data
    })
 }
 const failure =(res,code, message,data)=>{
     res.status(code).json({
         status:1,
         code:code || 400,
         message:message|| 'error',
         data
     })
 }
 module.exports={
 success,failure
 }
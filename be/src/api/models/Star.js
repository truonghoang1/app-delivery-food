const {Schema,model} = require("mongoose")
const Star = new Schema({
   type_star:{
    type:Number,
    enum:[0,1,2,3,4,5],
    default:0
   },
   id_product:{
    type:Schema.Types.ObjectId,
    ref:"Products",
    require:true
   },
   amount:{
    type:Number,
    default:1
   }
},{
    timestamps:true
})
module.exports = model("Star",Star)
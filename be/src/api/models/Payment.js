const {Schema,model} = require("mongoose")
const Payment = new Schema({
    product_order:[{
        type:Schema.Types.ObjectId,
        ref:"ProductsOrder",
        require:true
    }],
    total_pay:{
        type:Number,
        require:true
    },
    user_id:{
        type:Schema.Types.ObjectId,
        ref:"Users",
        require:true
    },
    my_discount:{
        type:Schema.Types.ObjectId,
        ref:"Discounts"
}
},{
    timestamps:true
})
module.exports = model("Payment",Payment)
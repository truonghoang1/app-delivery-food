const mongoose = require("mongoose")
const {Schema,model} = mongoose
const  Discounts = new Schema({
    status:{
        type:Boolean,
        default:true
    },
    code :{
        type:String,
        require:true
    },
    start_date:{
        type:String,
        require:true
    },end_date:{
        type:String,
        require:true
    },discount:{
        type:Number,
        require:true
    }
},{
    timestamps:true
})
module.exports = model("Discounts",Discounts)
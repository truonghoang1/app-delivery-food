const {Schema,model} = require("mongoose")
const ProductsOrder = new Schema({
    id_product:{
        type:Schema.Types.ObjectId,
        ref:"Products",
        require:true
    },
    name:{
        type:String,
        require:true
    }
    ,
    price:Number,
    amount:{
        type:Number,
        require:true
    }

},{timestamps:true})
module.exports = model("ProductsOrder",ProductsOrder)
const mongoose = require("mongoose")
const {Schema,model} = mongoose
const Products = new Schema({
    status:{
        type:Boolean,
        default:true
    },
    name:{
        type:String,
        require:true
    },
    price:{
        type:Number,
        require:true
    },star:[
        {type:Schema.Types.ObjectId,
            ref:"Star"}
    ],
    size:[
        {
            type:String,
            enum:["small","medium","large"]
        }
    ],description:{
        type:String
    },image:[
        {
            type:String
        }
    ],shortDescription:{
        type:String
    }
},{
    timestamps:true
})
module.exports = model("Products",Products)
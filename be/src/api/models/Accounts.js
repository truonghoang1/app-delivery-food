const {Schema,model} = require("mongoose")
const Accounts = new Schema({
    email:{
        type:String,
        require:true
    },password:{
        type:String,
        require:true
    },id_user:{
        type:Schema.Types.ObjectId,
        ref:"Users"
    }
},{
    timestamps:true
})
module.exports = model("Accounts",Accounts)
const {Schema,model} = require("mongoose")
const Users = new Schema({
    full_name:{
     type:String,
     require:true
    },phone:{
        type:String,
        require:true
    },
    dob:String,
    gender:String,
    address:String,
    location:{
        type:{
            type:String,
            enum:["Point","Polygon"],
            default:"Point"
        },
         coordinates: {
            type: [Number],
            
          },
        
    },
    product_like:[
        {type:Schema.Types.ObjectId}
    ],
    
    account:{
        type:Schema.Types.ObjectId,
        ref:"Accounts"
    }
},{
    timestamps:true
})
module.exports = model("Users",Users)
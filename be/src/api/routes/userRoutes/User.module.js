const express = require("express")
const router = express.Router()
const { verifyToken } = require("../../../middleware/auth.middleware")
const { verifyEmail, verifyPassword, verifyPhone } = require("../../../middleware/verify")
const userService = require("../../services/userServices/User.module")
router.route("/signup")
    .post(verifyEmail, verifyPassword, verifyPhone, userService.signup)
router.route('/signin')
    .post(userService.signin)

module.exports = router
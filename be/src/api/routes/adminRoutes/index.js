const express = require('express')
const router= express.Router()
const productModuleRoute =require("./Products.module")
router.use("/product",productModuleRoute)

module.exports = router
const express = require("express")
const router = express.Router()
const {verifyToken} = require("../../../middleware/auth.middleware")
const adminService = require("../../services/adminServices/Product.module")
router.route("/manager")
.get(adminService.getProduct)
.post(adminService.addProduct)

router.route("/manager/:id")
.get(adminService.getDetail)
.put(adminService.updateProduct)
.delete(adminService.deleteProduct)
module.exports =router
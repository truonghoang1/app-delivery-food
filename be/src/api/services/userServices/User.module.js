require("dotenv").config()
const { model } = require("mongoose")
const { failure, success } = require("../../../common/response.common")
const bcrypt = require("bcryptjs")
const jwt = require("jsonwebtoken")
const Users = model("Users"),
   Account = model("Accounts")
const signup = async (req, res) => {
   const { email, full_name, phone, address, gender, dob, password } = req.body
   let hashpassword = await bcrypt.hash(password, 10)

   const newUser = new Users(
      {
         email, full_name, phone, address, gender, dob, password: hashpassword
      }
   )
   newUser.save(async (err, data) => {
      if (err) return failure(res, 400, "error", {})
      const newAccount = new Account({
         email, password: hashpassword, id_user: data._id
      })
      await newAccount.save()
      success(res, 200, "success", data)
   })
}
const signin = async (req, res) => {
   const { email, password } = req.body
   console.log(req.body);
   let data = await Account.findOne({ email }, { __v: 0 })
   if (!data) return failure(res, 404, "not found email", {})
   const decoded = await bcrypt.compare(password, data.password)
   if (!decoded) return failure(res, 400, "password invalid")
   const token = generateToken({ data })
   success(res,200,"login success",{token})

}
const searchProduct = async(req,res)=>{
   
}


const signinWithOtp = (req, res) => {

}
function generateToken(user) {
   return jwt.sign(user, process.env.HASH_PASSWORD_SERVER, { expiresIn: "1800s" })
}

module.exports = {
   signup, signin
}
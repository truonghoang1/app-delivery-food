const {model} = require("mongoose")

const {success,failure} = require("../../../common/response.common")
const Products = model("Products"),
Star = model("Star")
const getProduct = async(req,res)=>{
    const {limit,} = req.query
    let parseLimit = Number.parseInt(limit)
    Products.find({}).limit(parseLimit).populate({
        path:"star",
        select:["type_star","amount"]
    }).exec((err,data)=>{
if(err) return failure(res,400,err,{})
 success(res,200,"success",data)
    })
}
const getDetail = async(req,res)=>{
    const {id}= req.params
    Products.findById({_id:id},{__v:0},(err,data)=>{
if(err) failure(res,400,err,{})
 success(res,200,"success",data)
    })

}
const addProduct = async (req,res)=>{
  try {
    const {name,image,price,description,shortDescription,size} = req.body
    
    
    const newProduct = new Products({
        name,price,image,description,shortDescription,size
    })
    newProduct.save((err,data)=>{
        if(err) return failure(res,400,"error",err)
        const starArr = [
            {type_star:1,id_product:data._id},
            {type_star:2,id_product:data._id},
            {type_star:3,id_product:data._id},
            {type_star:4,id_product:data._id},
            {type_star:5,id_product:data._id}
        ]
     Star.insertMany(starArr,(err1,data1)=>{
        if(err1) return failure(res,400,"error",err1)
        data.star =data1
        data.save()
        success(res,200,"success",data)
     })
        
    })

  } catch (error) {
    failure(res,500,"Server Error",{})
    
  }
}
const updateProduct = async (req,res)=>{
    const {options} = req.body
    const {id} = req.params
    Products.findByIdAndUpdate({_id:id},{$set:options},{new:true},(err,data)=>{
        if(err) failure(res,400,err,{})
        success(res,200,"success",data)
    })

}
const deleteProduct = async (req,res)=>{
    const {id} = req.params
    Products.findByIdAndDelete({_id:id},{new:true},(err,data)=>{
        if(err) failure(res,400,err,{})
       Star.deleteMany({id_product:data._id},{new:true},(err2,data2)=>{
        if(err2) failure(res,400,err2,{})
        success(res,200,"success",data)
       })
    })

}
module.exports =  {
    getProduct,addProduct,updateProduct,deleteProduct,getDetail
}

require('dotenv').config()
const express = require('express')
const cors = require('cors')
const path = require('path')
const mongose = require('mongoose')
require('./src/api/models')
const adminRoute = require("./src/api/routes/adminRoutes")
const userRoute = require("./src/api/routes/userRoutes")
const fileUpload = require('express-fileupload')
const app = express()
app.use(cors(
    {
        origin: '*'
    }
))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(fileUpload({
    useTempFiles: true,
    tempFileDir: '/tmp/'
}
))

const url = process.env.MOOGODB_ATLAS
// console.log(url);
mongose.Promise = global.Promise
app.use('/admin', adminRoute)
// app.use('/upload', routeImg)
app.use('/api', userRoute)
mongose.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}
).then(
    () => {
        console.log("Connected db")
    }
).catch(err => {
    console.log(err)
})


app.listen(process.env.PORT, () => {
    console.log(`Connect ${process.env.PORT}`);
})